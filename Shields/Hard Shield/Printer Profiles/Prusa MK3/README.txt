Make sure to adjust the print settings to your printer.  This file should be 
    used for plating/nesting the parts on a 210x250mm printbed. It was created
    with a 0.6mm nozzle and the corresponding settings, so please check and 
    adjust to fit your printer's needs.

File created by Andrew Gerth in association with Missouri S&T.