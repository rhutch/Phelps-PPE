Missouri S&T N95 Mask - Version B3

	Advisors: Dr. Christopher Ramsey, Richard Dalton
	Design and CAD: Eric Schneider, Andrew Parasch, Steve Williams, Max Foley, Tom Hoing, Drew Ellison  
	Manufacturing: Drew Ellison, Wes Hartzell, Spenser Walchuk, Ethan Peregoy

	Created in collaboration with Phelps Health in Rolla, MO.  Special thanks to Dr. Brian Kriete for design feedback.  Additional feedback is appreciated.

	*A huge thanks to everyone involved in the project.  Notably Jennifer Nixon for volunteering to coordinate communication.

**ALL MODELS (PAST, PRESENT, AND FUTURE) SHALL REMAIN OPEN SOURCE AND ACCESSIBLE TO THE PUBLIC***

RECOMMENDED PRINT SETTINGS FOR ENDER-3:
*Layer height MUST be 0.2mm for the Mask and Cap

MASK AND CAP
	Physical Properties:
		Layer Height: 0.2mm
		Wall Line Count: 3
		Top/Bottom Thickness: 0.6mm
		Top/Bottom Layers: 3
		Infill: 15%

	Temperatures in Celsius (Adjust as Needed):
		PLA:
			Print Temp: 225
			Bed Temp: 60
	
	Speeds (Adjust as Needed):
		Print Speed: 80 mm/s
		Infill Speed: 60 mm/s
		Outer Wall Speed: 60 mm/s
		Inner Wall Speed: 70 mm/s
		Top/Bottom Speed: 70 mm/s
		Travel Speed: 150 mm/s
		Initial Layer Speed: 50 mm/s

	Supports: None Needed

	Build Plate Adhesion: None

MASK DISK AND CAP DISK
	Physical Properties:
		Layer Height: 0.3mm
		Wall Line Count: 3
		Top/Bottom Thickness: 0.9mm
		Top/Bottom Layers: 3
		Infill: 15%

	Temperatures in Celsius (Adjust as Needed):
		PLA:
			Print Temp: 225
			Bed Temp: 60
	
	Speeds (Adjust as Needed):
		Print Speed: 90 mm/s
		Infill Speed: 80 mm/s
		Outer Wall Speed: 60 mm/s
		Inner Wall Speed: 70 mm/s
		Top/Bottom Speed: 70 mm/s
		Travel Speed: 150 mm/s
		Initial Layer Speed: 50 mm/s

	Supports: None Needed

	Build Plate Adhesion: None

	