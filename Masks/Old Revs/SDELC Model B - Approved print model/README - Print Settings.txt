Recommeded Print Settings for STEAM Mask B2

Optimised for Creality Ender-3 and Creality CR-10. If your printer is capable of printing faster, please do.

Front Disk, Sep Disk, and Cap:
Physical Properties:
	Layer Height: 0.3mm
	Wall Line Count: 3
	Top/Bottom Thickness: 0.9mm
	Top/Bottom Layers: 3
	Infill: 18%
Temperatures:
	Print Temp: As you see fit for your PLA
	Bed Temp: As you see fit for your PLA
	Infill Speed: 80 mm/s
	Outer Wall Speed: 55 mm/s
	Inner Wall Speed: 80 mm/s
	Top/Bottom Speed: 70 mm/s
	Travel Speed: 120 mm/s
	Initial Layer Speed: 50 mm/s
Supports: NO
Build Plate Adhesion: NONE

Mask:
Physical Properties:
	Layer Height: 0.2mm
	Wall Line Count: 3
	Top/Bottom Thickness: 0.6mm
	Top/Bottom Layers: 2
	Infill: 18%
Temperatures:
	Print Temp: As you see fit for your PLA
	Bed Temp: As you see fit for your PLA
	Infill Speed: 80 mm/s
	Outer Wall Speed: 55 mm/s
	Inner Wall Speed: 80 mm/s
	Top/Bottom Speed: 70 mm/s
	Travel Speed: 120 mm/s
	Initial Layer Speed: 50 mm/s
Supports: NO
Build Plate Adhesion: NONE
