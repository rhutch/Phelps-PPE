# Phelps Health PPE

The purpose of this repository is to create a central, publicly accessible location for versions of face mask and face shield designs procured by the Healthcare Support Group of Missouri S&T.

Contacts:

   - Project Lead: Dr. Chris Ramsay
   - Design Team Lead: Dr. Ryan Hutcheson
   - Coordinator: Jennifer Nixon

Sub-teams:

   Missouri S&T SDELC (MASK)
   - Advisor: Richard Dalton
   - Program Manager: Max Foley
   - Design Lead: Eric Schneider
   - Manufacturing Lead: Spencer Walchuk
   - Manufacturing 3D Printing Specialist: Stephen Williams

  ITRSS (MASK, SHIELDS)
   - Advisor: Mark Bookout
   - Design Lead (SOFT SHIELD): Buddy Scharfenberg
   - Design Lead (CASTING HARD SHIELD): Gordon Rhea

   Missouri S&T Makerspace (HARD SHIELD)
   - Advisor: Dr. Ryan Hutcheson
   - CAD and Design Lead: G. Fiedler
   - Manufacturing Lead and Design Support:  Daustin Hoelscher

Names and affiliations of all* personel who contributed to this project in some way are listed below:

|     Name              |    Affiliation                    |
|-----------------------|--------------------------         |
|Greg Hilmas		      |Missouri S&T                       |
|Mark	Bookout		      |Missouri S&T ITRSS                 |
|Buddy Scharfenberg	   |Missouri S&T                       |
|Rich	Wlezien			   |Missouri S&T                       |
|Casey Burton			   |Phelps Health                      |
|Shawn Hodges			   |Phelps Health                      |
|Joel	Burken			   |Missouri S&T                       |
|Steven Corns			   |Missouri S&T                       |
|Daustin	Hoelscher	   |Missouri S&T Makerspace            |
|Daryl Beetner			   |Missouri S&T                       |
|Krishna	Kolan			   |Missouri S&T                       |
|Frank Liou			      |Missouri S&T                       |
|Pourya Shamsi		      |Missouri S&T                       |
|Dennis Goodman		   |Missouri S&T Health Services       |
|Alec	Reven			      |Missouri S&T                       |
|Gordon Rhea			   |Missouri S&T ITRSS                 |
|Doug Roberts			   |Missouri S&T University Police     |
|Don Howdeshell			|Missouri S&T                       |
|Ashley Schmid			   |Missouri S&T                       |
|Reed Buie			      |Missouri S&T                       |
|Todd Sparks			   |Product Innovation and Engineering |
|Connor Coward			   |Product Innovation and Engineering |
|Stephen Williams		   |Missouri S&T SDELC                 |
|Mitchell Cottrell		|Missouri S&T                       |
|Beth Cudney			   |Missouri S&T                       |
|G. Fiedler		         |Missouri S&T Makerspace            |
|KC Chandrashekhara	   |Missouri S&T                       |
|Viktor Khilkevich		|Missouri S&T                       |
|Jeff Birt			      |Missouri S&T                       |
|Ryan Hutcheson	      |Missouri S&T Makerspace            |
|Jennifer Nixon			|Missouri S&T ITRSS                 |
|Christopher Ramsay	   |Missouri S&T SDELC                 |
|Richard	Dalton	      |Missouri S&T SDELC                 |
|Ming Leu			      |Missouri S&T                       |
|Alec Murchie			   |Missouri S&T                       |
|Eric Showalter			|Missouri S&T                       |
|Daoru Han			      |Missouri S&T                       |
|Heng Pan			      |Missouri S&T                       |
|Keith Nisbett			   |Missouri S&T                       |
|Eric Schneider			|Missouri S&T SDELC                 |
|Perry Tompkins			|Southwest Baptist University       |
|Thomas Wagner			   |Missouri S&T                       |
|Johannes Strobel	      |UMC                                |
|Perry Tompkins			|Southwest Baptist University       |
|Max Foley			      |Missouri S&T                       |
|Brian Kreite		      |Phelps Health                      |

*I will do my best to keep this updated, but the list may be incomplete.  I apologize if you aren't listed.

Document created by Daustin Hoelscher.
Last edited on 3.26.2020 at 1:07 A.M.



